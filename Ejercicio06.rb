class Product
  attr_reader :name, :prices
  def initialize(name, *prices)
    @name = name
    @prices = prices.map(&:to_i)
  end

  def average
    prices.inject(&:+) / @prices.size.to_f
  end
end


data = []
File.open('catalogo.txt', 'r') { |file| data = file.readlines.map(&:chomp) }

products_list = []
data.each do |prod|
  ls = prod.split(', ')
  products_list << Product.new(*ls)
end

puts 'Listado:'
products_list.each { |e| puts "#{e.name} => #{e.prices}"}

puts "\nPromedio de precios por producto:"
products_list.each { |e| puts "#{e.name} => #{e.average}"}