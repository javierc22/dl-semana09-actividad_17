class Product
  attr_reader :name, :prices
  def initialize(name, *prices)
    @name = name
    @prices = prices.map(&:to_i)
  end

  def average
    prices.inject(&:+) / @prices.size.to_f
  end

  def new_catalog
    "#{@name}, #{@prices[0..2]}".gsub(/\[|\]/, '') # gsub: elimina los "[]" del arreglo 'prices'
  end
end


data = []
File.open('catalogo.txt', 'r') { |file| data = file.readlines.map(&:chomp) }

products_list = []
data.each do |prod|
  ls = prod.split(', ')
  products_list << Product.new(*ls)
end

File.open('new_catalog.txt', 'w') { |file| file.puts products_list.map(&:new_catalog) }

puts 'Listado:'
products_list.each { |e| puts e.new_catalog }
